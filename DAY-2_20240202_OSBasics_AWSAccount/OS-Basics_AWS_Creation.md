#### Session Video:
   https://drive.google.com/file/d/1gbdbGF0Y_QG5Ux-EOqLX8XsRKgggp3SF/view?usp=sharing   

#### Topics:

   - Getting Started
   - OS Basics:
      
      - Unix Distributions : CLI (Open Source) & Convert CLI To GUI with CLI by installating 3rd Party Softwares 
      - Dennis R, Ken T and few others at AT & T bell Labs
      1960 Multics by 1972 Unix
         1. BSD
         2. AIX
         3. HP-UX 
         4. Solaris

      - DOS : CLI
      - MacOS : GUI & CLI : 1984
      - Windows : GUI & CLI : 1985
      - Linux Distributions : CLI (Open Source) & Convert CLI To GUI with CLI by installating 3rd Party Softwares
      
      Linux Kernel : Developed by Linus Torvalds in 1991 --> GNU

         - Ubuntu
         - CentOS
         - Redhat
         - AmazonLinux
         - Oracle Linux
         - Suse 


   - Types of Operating Systems
      - GUI & CLI/CUI
      - Only CLI/CUI

   
   - Creating Account with AWS 
      - Root Account 
   
      - https://www.youtube.com/watch?v=n0etTeHRJ4M
