#### Session Video:
   https://drive.google.com/file/d/1k7lfqnIEtTaw6GzwRZ4A0l71JpoyyYxP/view?usp=sharing

#### Topics:

   - Creating Account with AWS 
      - Root Account 

      - Account ID: 1913-2342-3716 --> Create Alias Name: "iam-cloubinary"

   - Login to AWS As Root  :
      
      https://aws.amazon.com/console/

      Root :
         UserName: 
         PassWord: 

Home Page: Console Home / Managment Console : GUI 


   - AWS Global infrastructure :
      - Regions: 33 + 4 = ? 
         - Physical Data Centers :
            - 105 Availability Zones(Data Centers) + 12 = ? 

      - News:
         - 4 more AWS Regions in Germany, Malaysia, New Zealand, and Thailand. 
         - 12 more Availability Zones 

   - AWS Services :
      200 fully featured services
   
   - AWS Services Types:
      
      - Global : 
         1. IAM
         2. Route53
      
      - Region :
         1. EC2
         2. RDS 

   - AWS Managment Console


AWS Services : Security, Identity, & Compliance -->  IAM 
      - User Mgmt  :
         - peterh
      - Group Mgmt :
         - No
      - Permissions :
            - AdministratorAccess Policy --> Attached a policy : Amazon Managed 

      - Policies :
         - AdministratorAccess Policy :
            - Permissions :
               - IAM (User Management)
               - EC2 (Virtual Machines in the cloud i.e. Windows, Linux, Unix & MacOS)
               - S3 (Storage)
               - RDS(Databases i.e. Oracle, MYSQL, MSSQL etc..) etc...

{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}

         

   --> IAM 

   - Create a User in AWS as root

   - Login to AWS as IAM user:

      Scenario-1 : https://iam-cloudbinary.signin.aws.amazon.com/console


      Scenario-2 : https://aws.amazon.com/console

      -> IAM user:
         191323423716
         peterh
         *****

      Scenario-3 :

      -> IAM user:
         iam-cloudbinary
         peterh
         *****

IAM --> peterh 

Roles and Responsibilities of peterh:

Role: Cloud Engineer

Responsibilities: Cloud Management 


Region : EC2  
