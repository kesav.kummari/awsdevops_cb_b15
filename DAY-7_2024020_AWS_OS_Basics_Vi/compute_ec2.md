#### Session Video:
    https://drive.google.com/file/d/1qFMc7YM6kPy95xeOPPz3XjiGKEXg_TvN/view?usp=sharing

#### Topics:

- AWS - Compute - EC2 :
    - Windows
    - Linux

- AWS CloudShell

- OS Basic Commands

- OS CLI/CUI Text Editors


#### 

STEP-1 : Login to AWS As Root User 

STEPT-2 : Reset IAM User Password (North Virginia us-east-1)
    
Console sign-in URL : https://cloudbinary-awsdevops.signin.aws.amazon.com/console
User name           : spiderman
Console password    : wU(=

STEP-3 : As an IAM user :
    - Create or Launch Virtual Machines in the Cloud i.e. AWS

    - EC2 (Region)
        - Windows
            - Connected Windows using RDP from Host Machine
        - Linux
            - Connected Linux using SSH tool i.e. GitBash from Host Machine

    - System Admin:
        - Basic Commands:
            - File & Directory Creation
            - Copy 
            - Move or Rename
            - Check CPU, RAM, Hard Disk & File System 
            
        - Advanced Commands:

STEP-4 : OS CLI/CUI Text Editors

    - vi and vim 




```


```

#### Commands List 

|Command|	Description|Example|
|---|---|---|
|ls|	List files and directories.	|ls or ls -l for detailed list|
|cd|	Change directory.	|cd /path/to/directory|
|pwd|	Print current working directory.	|pwd|
|mkdir|	Create a directory.	|mkdir directory_name|
|touch|	Create an empty file.	|touch file.txt|
|cp	|Copy files and directories.	|cp source_file.txt destination_folder/|
|mv	|Move or rename files and directories.	|mv file.txt new_location/ or mv old_name.txt new_name.txt|
|rm	|Remove files and directories.	|rm file.txt or rm -r directory/ to remove recursively|
|cat|	Concatenate and display file contents.	|cat file.txt|
|more|	Display file contents one page at a time.	|more file.txt|
|less|	Display file contents with backward and forward navigation.	|less file.txt|
|head|	Display the first lines of a file.	|head file.txt|
|tail|	Display the last lines of a file.	|tail file.txt|
|grep|	Search for patterns in files.	|grep "pattern" file.txt|
|find|	Search for files and directories.	|find /path/to/search -name "*.txt"|
|chmod|	Change file permissions.	|chmod 644 file.txt|
|chown|	Change file ownership.	|chown user:group file.txt|
|ssh|	Securely connect to a remote server.	|ssh user@hostname|
|wget|	Download files from the web.	|wget https://example.com/file.txt|
|tar|	Archive files and directories.	|tar -cvf archive.tar files/|
|gzip|	Compress files.	|gzip file.txt|
|unzip|	Extract files from a zip archive.	|unzip archive.zip|
|ps|	Display running processes.	|ps aux|
|kill|	Terminate a process.	|kill PID|
|top|	Monitor system processes and resource usage.	|top or htop (requires installation)|
|du|	Estimate file and directory disk usage.|du -sh directory/|
|df|	Display disk space usage.	|df -h|
|ifconfig|	Configure network interfaces (deprecated, use ip command).	|ifconfig|
|ping|	Send ICMP echo requests to a host.	|ping example.com|
|iptables|	Configure firewall rules (deprecated, use nft command).	|iptables -A INPUT -p tcp --dport 22 -j ACCEPT|
